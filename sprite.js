var nsg = require('node-sprite-generator');

nsg({
    compositor: 'jimp',
    src: [
        './images/sprites/*.png',
        './images/sprites/controls/*.png',
        './images/sprites/icons/*.png',
        './images/sprites/services/*.png'
    ],
    layout: 'packed',
    spritePath: './images/global/sprite.png',
    stylesheet: 'less',
    stylesheetPath: './less/sprite.less',
    stylesheetOptions: {
        spritePath: "../images/global/sprite.png"
    },
    layoutOptions: {
        padding: 4
    }
}, function (err) {
    console.log(err);
});/*
nsg({
    compositor: 'jimp',
    src: [
        './images/sprites/!*.png',
        './images/sprites/controls/!*.png',
        './images/sprites/icons/!*.png',
        './images/sprites/services/!*.png'
    ],
    layout: 'packed',
    spritePath: './images/global/sprite_x0_5.png',
    stylesheet: 'less_x0_5.tpl',
    stylesheetPath: './less/sprite_x0_5.less',
    stylesheetOptions: {
        nameMapping: function (imagePath) {
            var path = require('path');
            return path.basename(imagePath, path.extname(imagePath)) + '_x0_5';
        },
        spritePath: "../images/global/sprite_x0_5.png"
    },
    layoutOptions: {
        padding: 4,
        scaling: 0.5
    }
}, function (err) {
    console.log(err);
});*/